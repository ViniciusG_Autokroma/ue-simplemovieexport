// Copyright Epic Games, Inc. All Rights Reserved.

#include "SimpleMovieExportCommands.h"

#define LOCTEXT_NAMESPACE "FSimpleMovieExportModule"

void FSimpleMovieExportCommands::RegisterCommands()
{
	UI_COMMAND(PluginAction, "SimpleMovieExport", "Execute SimpleMovieExport action", EUserInterfaceActionType::Button, FInputChord());
}

#undef LOCTEXT_NAMESPACE

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "SimpleMovieExportStyle.h"

class FSimpleMovieExportCommands : public TCommands<FSimpleMovieExportCommands>
{
public:

	FSimpleMovieExportCommands()
		: TCommands<FSimpleMovieExportCommands>(TEXT("SimpleMovieExport"), NSLOCTEXT("Contexts", "SimpleMovieExport", "SimpleMovieExport Plugin"), NAME_None, FSimpleMovieExportStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > PluginAction;
};

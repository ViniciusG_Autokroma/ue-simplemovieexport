// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SimpleMovieExport : ModuleRules
{
	public SimpleMovieExport(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"MovieRenderPipelineCore",
				"MovieRenderPipelineEditor",
                "MovieSceneCaptureDialog",
            }
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"Projects",
				"InputCore",
				"EditorFramework",
				"UnrealEd",
				"ToolMenus",
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
			}
			);
		
	}
}
